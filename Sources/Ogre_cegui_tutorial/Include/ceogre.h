#pragma once
#include "BaseApplication.h"

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h> //from 0.8 it's just Ogre/Renderer.h
#include <OgreGLTextureCommon.h>

class BasicTutorial7 : public BaseApplication, public OIS::MouseListener, public OIS::KeyListener
{
public:
	BasicTutorial7(void);
	virtual ~BasicTutorial7(void);
	virtual int go(void);
protected:
	CEGUI::OgreRenderer* mRenderer;

	virtual void createScene(void);

	virtual void createFrameListener(void);

	// Ogre::FrameListener
	virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

	// OIS::KeyListener
	virtual bool keyPressed(const OIS::KeyEvent &arg);
	virtual bool keyReleased(const OIS::KeyEvent &arg);
	// OIS::MouseListener
	virtual bool mouseMoved(const OIS::MouseEvent &arg);
	virtual bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
	virtual bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

	bool quit(const CEGUI::EventArgs &e);

	Ogre::Root*                 mRoot;
	Ogre::Camera*               mCamera;
	Ogre::SceneManager*         mSceneMgr;
	Ogre::RenderWindow*         mWindow;
	Ogre::String                mResourcesCfg;
	Ogre::String                mPluginsCfg;
	Ogre::OverlaySystem*        mOverlaySystem;
	OgreBites::TrayManager*		mTrayMgr;
	OgreBites::CameraMan*		mCameraMan;     	// Basic camera controller
	OgreBites::ParamsPanel*     mDetailsPanel;   	// Sample details panel
	Ogre::String                m_ResourcePath;
private:
	OIS::InputManager*	mInputManager;
	OIS::Keyboard*		mKeyboard;
	OIS::Mouse*			mMouse;
	bool				mShutDown;
};