#ifndef __BaseApplication_h_
#define __BaseApplication_h_

#include <OgreApplicationContext.h>
#include <OgreInput.h>
#include <OgreTrays.h>
#include <OgreCameraMan.h>
#include <OgreSceneManager.h>
#include <OgreEntity.h>
#include <OgreShaderGenerator.h>
#include <OgreAdvancedRenderControls.h>
#include <OIS.h>
#include <OgreRoot.h>
#include <OgreTextureManager.h>
#include <OgreRenderTexture.h>

class BaseApplication : public OgreBites::ApplicationContext, public OgreBites::InputListener
{
public:
	BaseApplication(void);
	virtual ~BaseApplication(void);

	bool keyPressed(const OgreBites::KeyboardEvent& evt);
	void loadResources(void);
	void setup(void);
	int go(void);

private:
	OgreBites::ApplicationContext* appContext;
	OgreBites::InputListener* inputListener;
	OgreBites::TrayManager* trays;
	OgreBites::CameraMan* camMan;
	OgreBites::AdvancedRenderControls* ctrls;
};

#endif // __BaseApplication_h_
