#include "BaseApplication.h"

BaseApplication::BaseApplication(void)
{
	appContext = new OgreBites::ApplicationContext(OGRE_VERSION_NAME, false);
	inputListener = new OgreBites::InputListener();
	addInputListener(this);
}

BaseApplication::~BaseApplication(void)
{
	delete appContext;
	delete inputListener;
	delete trays;
	delete camMan;
	delete ctrls;
}

bool BaseApplication::keyPressed(const OgreBites::KeyboardEvent& evt)
{
	if (evt.keysym.sym == SDLK_ESCAPE)
	{
		getRoot()->queueEndRendering();
	}

	return true;
}

void BaseApplication::loadResources(void)
{
	enableShaderCache();

	// load essential resources for trays/loading bar
	Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup("Essential");
	createDummyScene();

	if (trays)
	{
		delete trays;
	}

	trays = new OgreBites::TrayManager("Interface", getRenderWindow());
	addInputListener(trays);

	// show loading progress
	trays->showLoadingBar(1, 0);
	OgreBites::ApplicationContext::loadResources();

	// clean up
	trays->hideLoadingBar();
	destroyDummyScene();
}

void BaseApplication::setup(void)
{
	OgreBites::ApplicationContext::setup();

	Ogre::Root* root = getRoot();
	Ogre::SceneManager* scnMgr = root->createSceneManager(Ogre::ST_GENERIC);

	Ogre::RTShader::ShaderGenerator* shadergen = Ogre::RTShader::ShaderGenerator::getSingletonPtr();
	shadergen->addSceneManager(scnMgr);

	// overlay/trays
	scnMgr->addRenderQueueListener(getOverlaySystem());
	trays->showFrameStats(OgreBites::TL_TOPRIGHT);
	trays->refreshCursor();

	// enable pixel per lighting
	Ogre::RTShader::RenderState* rs = shadergen->getRenderState(Ogre::RTShader::ShaderGenerator::DEFAULT_SCHEME_NAME);
	rs->addTemplateSubRenderState(shadergen->createSubRenderState(Ogre::RTShader::PerPixelLighting::Type));

	scnMgr->setAmbientLight(Ogre::ColourValue(0.1f, 0.1f, 0.1f));

	Ogre::Light* light = scnMgr->createLight("MainLight");
	light->setPosition(0, 10, 15);

	Ogre::Camera* cam = scnMgr->createCamera("myCam");
	cam->setNearClipDistance(5);
	cam->setAspectRatio(true);

	camMan = new OgreBites::CameraMan(cam);
	camMan->setStyle(OgreBites::CS_ORBIT);
	camMan->setYawPitchDist(Ogre::Radian(0), Ogre::Radian(0.3f), 15);
	addInputListener(camMan);

	// must keep a reference to ctrls so it does not get deleted (?)
	ctrls = new OgreBites::AdvancedRenderControls(trays, cam);
	addInputListener(ctrls);

	Ogre::Viewport* vp = getRenderWindow()->addViewport(cam);
	vp->setBackgroundColour(Ogre::ColourValue(0.3f, 0.3f, 0.3f));

	Ogre::Entity* ent = scnMgr->createEntity("ogrehead.mesh");
	Ogre::SceneNode* node = scnMgr->getRootSceneNode()->createChildSceneNode();
	node->attachObject(ent);
}

int BaseApplication::go(void)
{
	BaseApplication* app = new BaseApplication();
	app->initApp();
	app->getRoot()->startRendering();
	app->closeApp();

	return 0;
}
